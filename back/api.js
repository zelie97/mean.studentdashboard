const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
const cors = require("cors");

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/dashboarddb', { useNewUrlParser: true, autoIndex: false }, function (err) {
    if (err) { throw err; }
});

var Schema = mongoose.Schema;

var StudentSchema = new Schema({
    nickname: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    firstname: {
        type: String,
        require: true
    },
    lastname: {
        type: String,
        require: true
    },
    email: {
        type: String
    },
    section: {
        type: String
    },
    status : {
        type: Boolean, //Student 0, Teacher 1
        default: false,
        require: true
    },
    projects: { //Contain marks and comments
        type: [
            {
                projectName: String,
                comment: String,
                mark: Number
            }
        ]
    },
    delays : {
        type: Array
    },
    absences : {
        type: Array
    },
    comments: {
        type: Array
    }
});

var PendingStudentSchema = new Schema({
    nickname: {
        type: String,
        unique: true,
        require: true
    },
    firstname: {
        type: String,
        require: true
    },
    lastname: {
        type: String,
        require: true
    },
    section: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    }
});

var Student = mongoose.model('Student', StudentSchema);
var PendingStudent = mongoose.model('PendingStudent', PendingStudentSchema);

// Create an admin account
// var user = {
//     nickname: "admin",
//     password: "admin",
//     firstname: "admin",
//     lastname: "admin",
//     status: true
// }
// Student.create(user, function (err) {
//     if (err) {
//         throw err;
//     }
// });

// Create a teacher account
// var user = {
//     nickname: "thomas",
//     password: "thomas",
//     firstname: "Thomas",
//     lastname: "Jamais",
//     section: "Dev Web",
//     status: true
// }
// Student.create(user, function (err) {
//     if (err) {
//         throw err;
//     }
// });


//Add new student (by student's form)
app.post("/temporary/user", (req, res) => {
    var pendingUser = new PendingStudent(req.body);
    pendingUser.save(function (err) {
        if (err) return console.error(err);
    });
    res.send();
});

//Add new student (by teacher's form)
app.post("/new/user", (req, res) => {
    var user = new Student(req.body);
    user.save(function (err) {
        if (err) return console.error(err);
    });
    res.send();
});

//Send student's data to the front, for a student
app.get("/data/:nickname", async (req, res) => {
    var dashboard = await Student.find({ nickname : req.params.nickname });
    res.json(dashboard);
});

//Send all students in a section to the front, for the teacher
app.get("/all/:section", async (req, res) => {
    var dashboards = await Student.find({ section: req.params.section, status: false });
    res.send(dashboards);
});

//Send all pending students in a section to the front, for the teacher
app.get("/pending/:section", async (req, res) => {
    var pendingInSection = await PendingStudent.find({ section: req.params.section });
    res.send(pendingInSection);
});

//Send all different names of sections
app.get("/sections", (req, res) => {
    Student.find({ status: true }).distinct('section').then((sections) => {
        res.send(sections)
    });
}); 

//Return a teacher id
//app.get("/oneteacher/:section", async (req, res) => {
//    var idteacher =await Student.findOne({ section: req.params.section, status: true }, '_id')
//    .then((idteacher) => {
//        console.log(idteacher._id);
//        res.send(idteacher._id);
//    }).catch((err) => {
//        console.error(err);
//    })
//});

//Update data of a student, by the teacher
app.put("/edit/:nickname", (req, res) => {
    Student.findOneAndUpdate({ nickname: req.params.nickname }, { $set: req.body }, { new: true }).then((student) => {
        res.send(student);
    })
});

//Connection
app.post("/connection", (req, res) => {
    Student.findOne(req.body).then((connected) => {
        if (connected) {
            res.send(connected);
        } else {
            res.send("Incorrect user or password");
        }
    })
});

//Students not assignated
app.get("/notassignated/:section", async (req, res) => {
    var notAssignated = await Student.find({ section: req.params.section, status: false, email: undefined });
    res.send(notAssignated);
});

//Accept and remove a pending inscription
app.put("/merge/:id", (req, res) => {
    Student.findByIdAndUpdate({ _id: req.params.id }, { $set: req.body }, { new: true })
    .then((merged) => {
        PendingStudent.deleteOne({ nickname: merged.nickname }, function(err) {
            if (err) return handleError(err);
            res.send(merged);
        })
    })
});

//Add a new project to all students in a section
app.put("/project/:section", async (req, res) => {
    try {
    await Student.updateMany({ section: req.params.section }, { $push: {
        "projects": {"projectName": req.body.projectName} }});
        res.send(); }
    catch (error) {
        console.error(error);
    }
});

//Add a comment to a project
app.put("/project/comment/:id/:idproject", async (req, res) => {
    try {
    await Student.updateOne({ _id: req.params.id }, { $set: {
        "projects.$[i].comment": req.body.comment }}, { arrayFilters: [ { "i._id": req.params.idproject } ] });
        res.send(); }
    catch (error) {
        console.error(error);
    }
});

//Add a mark to a project
app.put("/project/mark/:id/:idproject", async (req, res) => {
    try {
    await Student.updateOne({ _id: req.params.id }, { $set: {
        "projects.$[i].mark": req.body.mark }}, { arrayFilters: [ { "i._id": req.params.idproject } ] });
        res.send(); }
    catch (error) {
        console.error(error);
    }
});

//Send a student's project to the front
app.get("/myproject/:id/:idproject", async (req, res) => {
    await Student.findOne({ _id: req.params.id, "projects._id": req.params.idproject }, { 'projects.$': 1 }, function (err, docs) {
        if(err){
            console.error(err);
         } else {
            console.log(docs.projects);
            res.send(docs.projects[0]);
         }
    })
});

//Add an absence
app.put("/absence/:id", (req, res) => {
    console.log(req.body.absenceDate);
    console.log(req.params.id);
    Student.findOneAndUpdate({ _id: req.params.id }, { $push: { "absences": req.body.absenceDate }},
    function(err) {
        if(err){
            console.error(err);
        } else {
        res.send();
        }
    })
});

//Add all section's projects to a new student of that section
app.get("/projects/:idteacher", async (req, res) => {
    var doc = await Student.findById(req.params.idteacher, "projects");
        if (doc.projects.length > 0) {
            console.log(doc.projects);
            res.json(doc.projects);
        } else {
            res.send([]);
        }
});

//A EFFACER
app.get("/all", async (req, res) => {
    var dashb = await Student.find({});
    res.send(dashb);
});

//A EFFACER
app.get("/allpending", async (req, res) => {
    var dashbos = await PendingStudent.find({});
    res.send(dashbos);
});

app.listen(3000);