import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front';

  private user: any;
  private pendingRegDone: boolean;
  private user_status: boolean;
  private user_nickname: String;
  private regComp = false;
  private logComp = false;

  constructor() { }

  onUser(event) {
    this.user = event;
    this.user_status = this.user.status;
    this.user_nickname = this.user.nickname;
  }

  onPending(event) {
    this.pendingRegDone = event;
    this.regComp = false;
  }

  logOut(event : Event) {
    this.user = null;
    this.user_nickname = null;
    this.logComp = false;
  }

  login() {
    this.logComp = true;
    this.regComp = false;
  }

  register() {
    this.regComp = true;
    this.logComp = false;
  }

}
