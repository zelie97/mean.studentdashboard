import { Component, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() loggedUser = new EventEmitter<any>();

  private user: any;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
  }

  onFormSubmit(userForm: NgForm) {
    if (userForm.value.login_nickname && userForm.value.login_password) {
      this.http.post('http://localhost:3000/connection', {
        nickname: userForm.value.login_nickname,
        password: userForm.value.login_password
      })
        .subscribe(
          res => {
            this.user = res;
            this.loggedUser.emit(this.user);
          },
          err => {
            console.log("Incorrect user or password");
          }
        )
    }
  }

}
