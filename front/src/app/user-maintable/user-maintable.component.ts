import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-user-maintable',
  templateUrl: './user-maintable.component.html',
  styleUrls: ['./user-maintable.component.scss']
})
export class UserMaintableComponent implements OnInit {

  @Input("user") user: any;

  private dashboard: any;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get_user_dashboard(this.user.nickname).subscribe((res: any) => {
      this.dashboard = res[0];
      //récupérer le nom et le prénom de l'user
    });
  }

}
