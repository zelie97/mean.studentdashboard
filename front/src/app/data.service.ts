import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  baseUrl: string = "http://localhost:3000";

  constructor(private httpClient: HttpClient) { }

  get_user_dashboard(nickname: String) {
    return this.httpClient.get(this.baseUrl + '/data/' + nickname);
  }
  
  get_users_in_section(section: String) {
    return this.httpClient.get(this.baseUrl + '/all/' + section);
  }

  get_pending_in_section(section: String) {
    return this.httpClient.get(this.baseUrl + '/pending/' + section);
  }

  get_sections() {
    return this.httpClient.get(this.baseUrl + '/sections');
  }

  get_not_assignated(section: String) {
    return this.httpClient.get(this.baseUrl + '/notassignated/' + section);
  }

  get_section_projects(idteacher: String) {
    return this.httpClient.get(this.baseUrl + '/projects/' + idteacher).toPromise();
  }

  get_student_project(id: String, idproject: String) {
    return this.httpClient.get(this.baseUrl + '/myproject/' + id + '/' + idproject);
  }

  get_id_teacher(section: String) {
    return this.httpClient.get(this.baseUrl + '/oneteacher/' + section);
  }

}
