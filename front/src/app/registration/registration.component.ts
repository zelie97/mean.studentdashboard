import { Component, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  @Output() pendingUser = new EventEmitter<any>();

  private pendingRegDone: boolean;
  private sections: any;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.get_sections().subscribe((res: any) => {
      this.sections = res;
    });
  }

  onFormSubmit(userForm: NgForm) {
    if (userForm.value.pending_nickname && userForm.value.pending_password && userForm.value.pending_firstname && userForm.value.pending_lastname && userForm.value.pending_email && userForm.value.pending_section) {
      this.http.post('http://localhost:3000/temporary/user', {
        nickname: userForm.value.pending_nickname,
        password: userForm.value.pending_password,
        firstname: userForm.value.pending_firstname,
        lastname: userForm.value.pending_lastname,
        email: userForm.value.pending_email,
        section: userForm.value.pending_section
      })
        .subscribe(
          res => {
            this.pendingRegDone = true;
            this.pendingUser.emit(this.pendingRegDone);
          },
          err => {
            console.log("Error occured");
          }
        )
    }
  }

}
