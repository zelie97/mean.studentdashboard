import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-admin-userstable',
  templateUrl: './admin-userstable.component.html',
  styleUrls: ['./admin-userstable.component.scss']
})
export class AdminUserstableComponent implements OnInit {

  @Input("teacher") teacher: any;

  private dashboard: any;
  private pendingUsers: any;
  private notAssignatedStudents: any;
  private selectedStudent: any;
  private selectedProject: any;
  private notAssignatedId: String;
  private addFirstname: String;
  private addLastname: String;
  private addProjectName: String;
  private addMark: String;
  private addComment: String;
  private dt: String;
  private projects: any;
  private myProject: any;

  constructor(private http: HttpClient, private dataService: DataService) { }

  ngOnInit() {
    this.getStudentsInSection(this.teacher.section);
    this.getPendingInSection(this.teacher.section);
    this.getStudentsNotAssignated(this.teacher.section);
  }

  getPendingInSection(section: String) {
    this.dataService.get_pending_in_section(section).subscribe((res: any) => {
      this.pendingUsers = res;
    })
  }

  async getStudentsInSection(section: string) {
    this.projects = await this.getSectionProjects(this.teacher._id);
    this.dataService.get_users_in_section(section).subscribe((res: any) => {
      this.dashboard = res;
    })
  }

  getStudentsNotAssignated(section: String) {
    this.dataService.get_not_assignated(section).subscribe((res: any) => {
      this.notAssignatedStudents = res;
    })
  }

  getStudentProject(id: String, idproject: String) {
    this.dataService.get_student_project(id, idproject).subscribe((res: any) => {
      this.myProject = res;
      console.log(this.myProject);
    })
  }

  async getSectionProjects(idteacher: String) {
    return await this.dataService.get_section_projects(idteacher);
  }

  generateID() {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  async onFormSubmit(userForm: NgForm) {
    if (userForm.value.firstname && userForm.value.lastname) {
      this.projects = await this.getSectionProjects(this.teacher._id);
      this.http.post('http://localhost:3000/new/user', {
        firstname: userForm.value.firstname,
        lastname: userForm.value.lastname,
        status: false,
        section: this.teacher.section,
        projects: this.projects,
        nickname: this.generateID()
      })
        .subscribe(
          res => {
            this.addFirstname = '';
            this.addLastname = '';
            this.getStudentsInSection(this.teacher.section);
            this.getPendingInSection(this.teacher.section);
            this.getStudentsNotAssignated(this.teacher.section);
          },
          err => {
            console.log("Error occured");
          }
        )
    }
  }

  onFormProjectSubmit(projectForm: NgForm) {
    if (projectForm.value.projectName) {
      this.http.put('http://localhost:3000/project/' + this.teacher.section, {
        projectName: projectForm.value.projectName
      })
        .subscribe(
          res => {
            this.addProjectName = '';
            this.getStudentsInSection(this.teacher.section);
          }
        )
    }
  }

  onFormAbsenceSubmit() {
    if (this.dt) {
      this.http.put('http://localhost:3000/absence/' + this.selectedStudent._id, {
        absenceDate: this.dt
    })
      .subscribe(
        res => {
          this.getStudentsInSection(this.teacher.section);
          //Ajouter le raffraichissement des retards
        }
      )
    }
  }

  onMarkFormSubmit(markForm: NgForm) {
    if (markForm.value.mark) {
      this.http.put('http://localhost:3000/project/mark/' + this.selectedStudent._id + '/' + this.selectedProject._id, {
        mark: markForm.value.mark
      })
      .subscribe(
        res => {
          this.addMark = '';
          this.getStudentProject(this.selectedStudent._id, this.selectedProject._id);
        }
      )
    }
  }

  onCommentFormSubmit(commentForm: NgForm) {
    if (commentForm.value.comment) {
      this.http.put('http://localhost:3000/project/comment/' + this.selectedStudent._id + '/' + this.selectedProject._id, {
        comment: commentForm.value.comment
      })
      .subscribe(
        res => {
          this.addComment = '';
          this.getStudentProject(this.selectedStudent._id, this.selectedProject._id);
        }
      )
    }
  }

  getNotAssignatedId(id: String) {
    this.notAssignatedId = id;
  }

  getStudent(student: any) {
    this.selectedStudent = student;
  }

  getProject(project: any) {
    this.selectedProject = project;
    this.getStudentProject(this.selectedStudent._id, this.selectedProject._id);
  }

  merge(pendingStud: any) {
    if (this.notAssignatedId) {
      this.http.put('http://localhost:3000/merge/' + this.notAssignatedId, {
        email: pendingStud.email,
        nickname: pendingStud.nickname,
        password: pendingStud.password
      })
      .subscribe(
        res => {
          this.getStudentsInSection(this.teacher.section);
          this.getPendingInSection(this.teacher.section);
          this.getStudentsNotAssignated(this.teacher.section);
        },
        err => {
          console.log("Error occured");
        }
      )
    }
  }

}
